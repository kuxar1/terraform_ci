variable "vpc_name" {
  type        = string
  description = "VPC name"
  default     = "main"

}

variable "cidr_block" {
  type        = string
  description = "VPC cidr block"
  default     = "192.168.0.0/16"
}

variable "igw_name" {
  type        = string
  description = "Internet gateway name"
  default     = "main"
}

variable "subnet_cidr_block" {
  type        = string
  description = "Subnet cidr block"
  default     = "192.168.1.0/24"
}

variable "subnet_name" {
  type        = string
  description = "Subnet name"
  default     = "public"
}

variable route_table_name {
  type        = string
  default     = "main"
  description = "route talbe name"
}
