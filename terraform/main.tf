terraform {
  backend "s3" {
    bucket         = "kuxar-terraform-state"
    key            = "test/s3/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-state"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source            = "./modules/vpc"
  vpc_name          = var.vpc_name
  cidr_block        = var.cidr_block
  igw_name          = var.igw_name
  subnet_cidr_block = var.subnet_cidr_block
  subnet_name       = var.subnet_name
}
