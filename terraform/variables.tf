variable "vpc_name" {
  type        = string
  description = "VPC name"
  default     = "demo_vpc"
}

variable "cidr_block" {
  type        = string
  description = "VPC cidr block"
  default     = "10.0.0.0/16"
}

variable "igw_name" {
  type        = string
  description = "Internet gateway name"
  default     = "demo_igw"
}

variable "subnet_cidr_block" {
  type        = string
  description = "Subnet cidr block"
  default     = "10.0.1.0/24"
}

variable "subnet_name" {
  type        = string
  description = "Subnet name"
  default     = "demo_subnet"
}

variable route_table_name {
  type        = string
  default     = "demo_rt"
  description = "route table name"
}
